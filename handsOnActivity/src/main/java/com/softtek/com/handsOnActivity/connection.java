package com.softtek.com.handsOnActivity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

public class connection {
	private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

	public void readDataBase() throws Exception {
		System.out.println("MySQL JDBC Connection");
        List<Devices> result = new ArrayList<>();
        String SQL_SELECT = "Select * from Device";
       
        	 try (Connection conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/sesion3?serverTimezone=UTC#", "root", "1234")) {
                 if (conn != null) {
                     System.out.println("Connected to the database!");
                     PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT);
                     ResultSet resultSet = preparedStatement.executeQuery();
                     while (resultSet.next()) {                		
                    	 int DeviceId=resultSet.getInt("DeviceID");
                    	 String Name=resultSet.getString("Name");
                    	 String Description=resultSet.getString("Description");
                    	 int ManufacturerID=resultSet.getInt("ManufacturerID");
                    	 int ColorId=resultSet.getInt("ColorID");
                    	 String Comments=resultSet.getString("Comments");
                    	 Devices de=new Devices(DeviceId,Name,Description, ManufacturerID,ColorId,Comments) ;
                         
                        //result.add(manu);
                         System.out.println("ID: " + DeviceId + " Device's name: " + Name + " Description: " + Description+"Manufacturer ID:"+ ManufacturerID+"Color´s ID: "+ColorId+"Comments: "+Comments);
                     }
                 } else {
                     System.out.println("Failed to make connection!");
                 }
                 result.forEach(System.out::println);
             } catch (SQLException e) {
                 System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
             } catch (Exception e) {
                 e.printStackTrace();
             }   
        
}
	 private void close() {
         try {
             if (resultSet != null) {
                 resultSet.close();
             }

             if (statement != null) {
                 statement.close();
             }

             if (connect != null) {
                 connect.close();
             }
         } catch (Exception e) {

         }
     }

	}
