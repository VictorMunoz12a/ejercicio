package com.softtek.com.handsOnActivity;

public class Devices {
	
private int DeviceId;
private String Name;
private String Description;
private int ManufacturerID;
private int ColorId;
private String Comments;



public Devices(int deviceId, String name, String description, int manufacturerID, int colorId, String comments) {
	DeviceId = deviceId;
	Name = name;
	Description = description;
	ManufacturerID = manufacturerID;
	ColorId = colorId;
	Comments = comments;
}
public int getDeviceId() {
	return DeviceId;
}
public void setDeviceId(int deviceId) {
	DeviceId = deviceId;
}
public String getName() {
	return Name;
}
public void setName(String name) {
	Name = name;
}
public String getDescription() {
	return Description;
}
public void setDescription(String description) {
	Description = description;
}
public int getManufacturerID() {
	return ManufacturerID;
}
public void setManufacturerID(int manufacturerID) {
	ManufacturerID = manufacturerID;
}
public int getColorId() {
	return ColorId;
}
public void setColorId(int colorId) {
	ColorId = colorId;
}
public String getComments() {
	return Comments;
}
public void setComments(String comments) {
	Comments = comments;
}






}
